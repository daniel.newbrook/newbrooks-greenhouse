#!/usr/bin/env python

import rospy
from newbrook_greenhouse.msg import Sens

import time
import os
import yaml

#Class DS18B20 is the class used to interface with the DS18B20 temperature sensors
class DS18B20:
    #On initialisation dev_ID give the ID for the device in question and tries to connect, if the device file does not exit - device is not connected and Runtime error is raised
    def __init__(self,dev_ID):
        base_dir='/sys/bus/w1/devices/'
        device_folder = (base_dir + '28-' + dev_ID)
        self.device_file = device_folder + '/temperature'
        if (not os.path.exists(self.device_file)):
            raise RuntimeError('ERROR: Device does not exist')

    #function read_temp_raw reads the device file defined in initialisation of class, this function is called by read_temp to read the current temperature
    def read_temp_raw(self):
        if (not os.path.exists(self.device_file)):
            print('ERROR: Device does not exist')
        self.f = open(self.device_file,'r')
        self.lines = self.f.readlines()
        self.f.close()
        return self.lines

    #function read_temp reads the current temperature of this temperature sensor
    def read_temp(self):
        lines = self.read_temp_raw()
        temp_c = float(lines[0]) / 1000.0
        return temp_c

def find_devices():
    # function that finds host name and uses file 'hardwareconfig.yaml' to find
    # which devices are connected to this pi
    # this function returns a list of dictionaries of lists of dictionaries
    # data structure is {host : {sensor : {s1 : ID, s2 : ID}, }, }
    my_host=os.uname()[1];
    with open(r'hardwareconfig.yaml') as file:
        hw_list = yaml.load(file)
        return hw_list[my_host]

def talker():
    T_sens={}
    H_sens={}
    F_sens={}
    L_sens={}
    M_sens={}
    on_device=find_devices()

    #Populate sensor lists based on devices that should be connected according to 
    #hardwareconfig.yaml
    for x in ['T','H','F','L','M']:
        if on_device[x+'_sensor']!=None:
            for t in on_device[x+'_sensor'].keys():
                if x=='T':
                    print('T')
                    T_sens[t] = DS18B20(str(on_device[x+'_sensor'][t]))
                elif x=='H':
                    print('H')
                elif x=='F':
                    print('F')
                elif x=='L':
                    print('L')
                elif x=='M':
                    print('M')

    #Setup ros publisher, publishes to sensor topic
    pub=rospy.Publisher('sensors', Sens, queue_size=10)
    rospy.init_node('sensor_node', anonymous=True)
    rate = rospy.Rate(0.5)
    msg = Sens()

    #While ros is running publish sensor data to sensor topic
    #Sensor message is string name, float value
    while not rospy.is_shutdown():
        for sens in T_sens.keys():
            msg.name=sens
            msg.value = T_sens[sens].read_temp()
            rospy.loginfo(msg)
            pub.publish(msg)
            rate.sleep()
        for sens in H_sens.keys():
            print('TODO')
        for sens in F_sens.keys():
            print('TODO')
        for sens in L_sens.keys():
            print('TODO')
        for sens in M_sens.keys():
            print('TODO')
#END of talker function

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
