
(cl:in-package :asdf)

(defsystem "newbrook_greenhouse-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Num" :depends-on ("_package_Num"))
    (:file "_package_Num" :depends-on ("_package"))
    (:file "Sens" :depends-on ("_package_Sens"))
    (:file "_package_Sens" :depends-on ("_package"))
  ))