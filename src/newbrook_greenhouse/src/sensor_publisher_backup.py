#!/usr/bin/env python

import rospy
from newbrook_greenhouse.msg import Sens

import time
import os
import yaml

class DS18B20:
    def __init__(self,dev_ID):
        base_dir='/sys/bus/w1/devices/'
        device_folder = (base_dir + '28-' + dev_ID)
        self.device_file = device_folder + '/w1_slave'
        if (not os.path.exists(self.device_file)):
            raise RuntimeError('ERROR: Device does not exist')

    def read_temp_raw(self):
        if (not os.path.exists(self.device_file)):
            print('ERROR: Device does not exist')
        self.f = open(self.device_file,'r')
        self.lines = self.f.readlines()
        self.f.close()
        return self.lines

    def read_temp(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
        return temp_c

def find_devices():
    my_host=os.uname()[1];
    with open(r'hardwareconfig.yaml') as file:
        hw_list = yaml.load(file)
        return hw_list[my_host]

def talker():
    T_sens={}
    H_sens={}
    F_sens={}
    L_sens={}
    M_sens={}

    on_device=find_devices()
    if on_device['T_sensor']!=None:
        for t in on_device['T_sensor'].keys():
            T_sens[t] = DS18B20(str(on_device['T_sensor'][t]))
    pub=rospy.Publisher('sensors', Sens, queue_size=10)
    rospy.init_node('sensor_node', anonymous=True)
    rate = rospy.Rate(2)
    msg = Sens()
    while not rospy.is_shutdown():
        for sens in T_sens.keys():
            msg.name=sens
            msg.value = T_sens[sens].read_temp()
            rospy.loginfo(msg)
            pub.publish(msg)
            rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
