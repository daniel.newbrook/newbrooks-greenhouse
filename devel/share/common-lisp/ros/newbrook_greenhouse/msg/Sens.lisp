; Auto-generated. Do not edit!


(cl:in-package newbrook_greenhouse-msg)


;//! \htmlinclude Sens.msg.html

(cl:defclass <Sens> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (value
    :reader value
    :initarg :value
    :type cl:float
    :initform 0.0))
)

(cl:defclass Sens (<Sens>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Sens>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Sens)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name newbrook_greenhouse-msg:<Sens> is deprecated: use newbrook_greenhouse-msg:Sens instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <Sens>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader newbrook_greenhouse-msg:name-val is deprecated.  Use newbrook_greenhouse-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <Sens>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader newbrook_greenhouse-msg:value-val is deprecated.  Use newbrook_greenhouse-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Sens>) ostream)
  "Serializes a message object of type '<Sens>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'value))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Sens>) istream)
  "Deserializes a message object of type '<Sens>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'value) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Sens>)))
  "Returns string type for a message object of type '<Sens>"
  "newbrook_greenhouse/Sens")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Sens)))
  "Returns string type for a message object of type 'Sens"
  "newbrook_greenhouse/Sens")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Sens>)))
  "Returns md5sum for a message object of type '<Sens>"
  "db3486341a840969e8845548decf6f11")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Sens)))
  "Returns md5sum for a message object of type 'Sens"
  "db3486341a840969e8845548decf6f11")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Sens>)))
  "Returns full string definition for message of type '<Sens>"
  (cl:format cl:nil "string name~%float32 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Sens)))
  "Returns full string definition for message of type 'Sens"
  (cl:format cl:nil "string name~%float32 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Sens>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Sens>))
  "Converts a ROS message object to a list"
  (cl:list 'Sens
    (cl:cons ':name (name msg))
    (cl:cons ':value (value msg))
))
