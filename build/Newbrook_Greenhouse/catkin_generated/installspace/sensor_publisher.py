#!/usr/bin/env python2

import rospy
from std_msgs.msg import String

import time
import os

class DS18B20:
    def __init__(self,dev_ID):
        base_dir='/sys/bus/w1/devices/'
        device_folder = (base_dir + '28-' + dev_ID)
        self.device_file = device_folder + '/w1_slave'
        if (not os.path.exists(self.device_file)):
            raise RuntimeError('ERROR: Device does not exist')

    def read_temp_raw(self):
        if (not os.path.exists(self.device_file)):
            print('ERROR: Device does not exist')
        self.f = open(self.device_file,'r')
        self.lines = self.f.readlines()
        self.f.close()
        return self.lines

    def read_temp(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
        return temp_c

def talker():
    T_sens = DS18B20('0119272857f0')
    pub=rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('sensor_node', anonymous=True)
    rate = rospy.Rate(2)
    while not rospy.is_shutdown():
        temp_string = T_sens.read_temp()
        rospy.loginfo(temp_string)
        pub.publish(str(temp_string))
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
