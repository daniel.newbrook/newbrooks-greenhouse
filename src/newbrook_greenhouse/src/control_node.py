#!/usr/bin/env python

import rospy

from newbrook_greenhouse.msg import Sens

T_sens={}

def callback(data):
    rospy.loginfo(rospy.get_caller_id()+ " i heard %s from %s", data.value,data.name)
    T_sens[data.name]=data.value
    print(T_sens[data.name])

def listener():
    rospy.init_node('control_node',anonymous=True)
    rospy.Subscriber('sensors',Sens,callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
